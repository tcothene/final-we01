# Final WE01 - Sujet n°2

## La complexité du Web

Comme le mentionne Stéphane Bortzmeyer, le Web d'aujourd'hui est de plus en plus complexe. À titre d'exemple, en 2020, 50 milliards d'équipements (ordinateurs, tablettes, smartphones, montres, etc.) étaient connectés à Internet **[1]** et 63.000 recherches sont effectuées sur Google chaque seconde **[2]**, ce qui souligne bien la grandeur et donc la complexité du Web. De plus, comme nous le savons, nos ordinateurs deviennent de plus en plus performants et malheureusement les applications qu'ils utilisent deviennent de plus en plus consommatrices, sans pour autant que l'utilisateur le sache. En effet, comme mentionné dans son article, M. Bortzmeyer mentionne le fait que les navigateurs les plus utilisés vont effectuer "d'innombrables opérations", sur notre machine ou sur des machines distances, pour simplement nous afficher la page que l'on souhaite consulter, tout en récupérant des données utilisateurs à notre sujet.

Le projet Gemini, tel que décrit par M. Bortzmeyer, est une alternative au Web, transparente et simple, n'affichant que des pages simples (pas de CSS, de Javascript ou même d'images) et ne transmettant pas des données utilisateurs. Ce dernier permet de lutter contre l'utilisation de nos données personnelles contre notre gré, le tout en se concentrant sur le contenu des sites web et non la forme. En proposant un tel système, cela permettrait aux utilisateurs qu'ils le souhaitent d'utiliser un navigateur très peu gourmand en ressources et qui ne fuite pas nos informations personnelles. Ce genre d'initiative est vraiment intéressante d'autant plus qu'elle est d'actualité puisque de plus en plus d'utilisateurs prennent conscience de l'importance de leurs données personnelles et car d'autres souhaitent bannir l'utilisation de logiciels et applications des GAFAMs en se tournant vers des alternatives libres comme Framasoft par exemple.

## Les chaînes éditoriales dans le cadre du projet Gemini

Il est intéressant de rapprocher le concept des chaînes éditioriales au projet Gemini de Stéphane Bortzmeyer puisque l'un comme l'autre porte prône la simplicité et porte davantage d'importance au fond plutôt qu'à la forme. En effet, une chaîne éditioriale XML va par exemple à partir d'un contenu XML (**fond**), produire plusieurs supports de **formes** différentes (diapositives, sites web, document papier, etc.). De l'autre côté, Gémini se veut simple et efficace en ne communiquant et en affichant que l'essentiel.

Ainsi, on pourrait imaginer une chaîne éditioriale, capable de produire des documents, compatibles avec Gemini, de formats différents, afin de faciliter la création de contenu sur ce système alternatif au Web. En faisant cela, cela pourrait également inciter certains utilisateurs à devenir créateur de contenu sur ce genre de plateforme ce qui aurait leur permettrait d'y contribuer et donc d'agrandir implicitement la plateforme.

## Choix de la licence

Pour ce rendu, j'ai choisi d'y associer la licence CC BY-SA qui permet de partager, de copier, distribuer et communiquer le document par tous moyens et sous tous formats ainsi qu'adapter, remixer, transformer et créer à partir du document pour toute utilisation, y compris commerciale.

En outre, si des modifications ont été effectué à partir de l'oeuvre, il nécessaire de créditer l'auteur et il est également nécessaire de partager l'oeuvre modifié avec la même licence que la licence originelle.

## Sources 
[1] [How Many Internet Connections are in the World? Right. Now.](https://blogs.cisco.com/news/cisco-connections-counter) Cisco Blogs
[2] [63 Fascinating Google Search Statistics](https://seotribunal.com/blog/google-stats-and-facts/) SEO Tribunal

*Rédigé par Thibault Cothenet*
